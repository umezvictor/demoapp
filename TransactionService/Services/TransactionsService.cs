﻿using Bogus;
using TransactionService.DataLayer.Model;
using TransactionService.DataLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionService.DataLayer.Dto;

namespace TransactionService.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly IUnitOfWork _unitOfWork;


        public TransactionsService(IUnitOfWork unitOfWork)
        {

            _unitOfWork = unitOfWork;
            
        }

        public async Task<List<Transaction>> GetAllAsync()
        {
            var transactions = await _unitOfWork.TransactionRepository.GetList();
            return transactions.ToList();
        }

        public async Task InsertAsync(Transaction transaction)
        {
          
           
            await _unitOfWork.TransactionRepository.CreateAsync(transaction);
            await _unitOfWork.SaveChangesAsync();
            
   
        }


        public async Task<Transaction> GetSingleAsync(string Id)
        {
            var student = await _unitOfWork.TransactionRepository.FindSingleAsync(x => x.Id == Guid.Parse(Id));
           
            return student;

            //exeption handler would have been implemented if I had more time
        }

    }
}
