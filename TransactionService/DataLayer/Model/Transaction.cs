﻿using TransactionService.DataLayer.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransactionService.DataLayer.Model
{
    public class Transaction : ISoftDeletable, ITrackable
    {
        public Guid Id { get; set; }
        public string ClientId { get; set; }
        public string WalletAddress { get; set; }
        public string CurrencyType { get; set; } 
        public DateTime TransactionDate { get; set; } 
        public decimal Amount { get; set; } 
        public decimal Balance { get; set; }
        
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset LastUpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
        public Guid RequestId { get; set; }

        public Request Request { get; set; }


    }


}
