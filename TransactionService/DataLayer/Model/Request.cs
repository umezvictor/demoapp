﻿using TransactionService.DataLayer.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionService.DataLayer.Model
{
    public class Request : ISoftDeletable, ITrackable
    {
        public Guid RequestId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset LastUpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
