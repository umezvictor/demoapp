﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TransactionService.DataLayer.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        ITransactionRepository TransactionRepository { get; }
        IRequestRepository RequestRepository { get; }
       
        Task<bool> SaveChangesAsync();
    }
}
