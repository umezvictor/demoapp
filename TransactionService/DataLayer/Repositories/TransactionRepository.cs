﻿using TransactionService.DataLayer;
using TransactionService.DataLayer.Model;
using TransactionService.DataLayer.Model.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransactionService.DataLayer.Repositories.Interfaces;

namespace TransactionService.DataLayer.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(AppDbContext context) : base(context)
        {

        }

        
    }
}
