﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransactionService.DataLayer.Dto
{
    

    public class Transactions
    {
        public string transactionId { get; set; }
        public string walletAddress { get; set; }
        public string currencyType { get; set; }
        public decimal amount { get; set; }
        public decimal balance { get; set; }
        public DateTimeOffset createdAt { get; set; }
        public DateTimeOffset lastUpdatedAt { get; set; }
        public object createdBy { get; set; }
        public object lastUpdatedBy { get; set; }
        public string clientId { get; set; }
        public object client { get; set; }
    }


    public class Message
    {
        public List<Transactions> transactions { get; set; }
    }

    public class TransactionsApiResponse
    {
        public Message message { get; set; }
    }

    public class TransactionReceived
    {
        public string message { get; set; }
    }

    
}
