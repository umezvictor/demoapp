﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionService.DataLayer.Repositories;
using TransactionService.DataLayer.Repositories.Interfaces;

namespace TransactionService.DataLayer
{
    public class DataLayerDependency
    {
        public static void AllDependency(IServiceCollection services)
        {
            
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
