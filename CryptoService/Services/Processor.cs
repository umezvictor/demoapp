﻿using CryptoService.DataLayer.Model;
using MassTransit;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoService.Services
{
   //processes incomming requests from the transaction service
   //returns response
    public class Processor : IConsumer<UpdateTransactions>
    {
        private readonly ITransactionService _transactionService;

        public Processor(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        public async Task Consume(ConsumeContext<UpdateTransactions> context)
        {
            var response = await _transactionService.GetAllAsync(context.Message.ClientId.ToString(), context.Message.WalletAddress.ToString());

            List<ClientTransaction> transactionsList = new List<ClientTransaction>();

            foreach(var item in response)
            {
                var obj = new ClientTransaction();
                obj.Amount = item.Amount;
                obj.Balance = item.Balance;
                obj.ClientId = item.ClientId.ToString();
                obj.CreatedAt = item.CreatedAt;
                obj.WalletAddress = item.WalletAddress;
                obj.TransactionId = item.TransactionId.ToString();
                transactionsList.Add(obj);
            }

            var transResponse = new TransactionResponse();
            transResponse.Transactions = transactionsList;

            await context.RespondAsync<TransactionResponse>(transResponse);
        }
    }
}
