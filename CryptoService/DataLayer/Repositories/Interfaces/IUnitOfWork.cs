﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService.DataLayer.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        ITransactionRepository TransactionRepository { get; }
       
        Task<bool> SaveChangesAsync();
    }
}
