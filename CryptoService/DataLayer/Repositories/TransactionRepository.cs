﻿using CryptoService.DataLayer;
using CryptoService.DataLayer.Model;
using CryptoService.DataLayer.Model.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CryptoService.DataLayer.Repositories.Interfaces;

namespace CryptoService.DataLayer.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(AppDbContext context) : base(context)
        {

        }

        
    }
}
