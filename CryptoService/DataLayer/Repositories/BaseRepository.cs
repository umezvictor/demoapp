﻿using CryptoService.DataLayer;
using CryptoService.DataLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService.DataLayer.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly AppDbContext _context;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(T entry)
        {
            await _context.Set<T>().AddAsync(entry);
        }

        public async Task CreateRangeAsync(List<T> entryList)
        {
            await _context.Set<T>().AddRangeAsync(entryList);
        }

        public void Delete(T entry)
        {
            _context.Set<T>().Remove(entry);
        }

        public void DeleteRange(List<T> entryList)
        {
            _context.Set<T>().RemoveRange(entryList);
        }

        public async Task<T> FindSingleAsync(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(expression);
        }

        public async Task<List<T>> GetList(Expression<Func<T, bool>> expression = null)
        {
            return  expression != null ? await _context.Set<T>().AsQueryable().Where(expression).AsNoTracking().ToListAsync() : await _context.Set<T>().AsQueryable().AsNoTracking().ToListAsync();

        }

        public IQueryable<T> QueryAll(Expression<Func<T, bool>> expression = null)
        {
            //basically - if expression is not null - run query using where claus, else run without where clause
            return expression != null ? _context.Set<T>().AsQueryable().Where(expression).AsNoTracking() : _context.Set<T>().AsQueryable().AsNoTracking();
        }

        public void Update(T entry)
        {
             _context.Set<T>().Update(entry);
        }

        public void UpdateRange(List<T> entryList)
        {
            _context.Set<T>().UpdateRange(entryList);
        }

    }
}
