﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoService.DataLayer.Repositories;
using CryptoService.DataLayer.Repositories.Interfaces;

namespace CryptoService.DataLayer
{
    public class DataLayerDependency
    {
        public static void AllDependency(IServiceCollection services)
        {
            
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
